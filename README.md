<b>##Intializing Git##</b><br/>
-Make a remote repo in Gitlab<br/>
-Change into the directory you wish to use<br/>
-"git init"<br/>
-"git remote add origin [your remote URL]"<br/>


<b>##Creating Files##</b><br/>
-Create 2 files in your directory<br/>
-"git status" will let you see that there are now 2 untracked changes<br/>
-You have two choices now to add these files;<br/>
--"git add ." will add all untracked files with changes to your repository<br/>
--"git add [file path/name]" will add the file at the specific path<br/>
--The latter is always the safer method if you are unsure on what to do<br/>


<b>##Committing Files##</b><br/>
-With the files added try "git commit"<br/>
-This will open up your text editor with all the changes, add your commit message, save and quit<br/>
-- **The "-m" option will allow you to add a message to the commit without a text editor<br/>
-- **The "-a" option will auto add any changed *TRACKED* files to your commit<br/>
-- **The "--amend" option will allow you to edit your most recent commit<br/>


<b>##Pushing your Repository##</b><br/>
-Always set a remote repository, we have done this already<br/>
-"git push origin master" will push this change up<br/>


<b>###This is the process to make changes in your code base###</b><br/>


<b>##Conflicts##</b><br/>
-There will be situations where you and another user are working on the same file<br/>
-When that happens you might find that changes you have made conflict with theirs<br/>
-Git has a useful tool to deal with these problems, merge<br/>
-Browse to your repository and edit [file1] and add 2 lines of text to it<br/>
-In your text editor, update [file1]<br/>
-"git fetch" to get the most recent information from your repo<br/>
-Try and "git commit -am 'conflict test'" & "git push origin master"<br/>
-Git will tell you that you are behind the remote branch, this is because your edit made a commit.<br/>
-"git pull origin master" to pull your most recent changes, Git will tell you an auto merge has failed<br/>
-- ** "git merge --abort" will allow you to abandon a merge if you started this by mistake<br/>
-"git status" will show you which files have the conflict<br/>
-Open conflicting files with your text editor, you will see identifiers for local(HEAD) and remote<br/>
-Update the files to resolve the merge<br/>
-"git add [conflictFile]" & "git commit -m 'Merge'" & "git push origin master"<br/>
-This will have corrected the conflict and pushed upto your final change<br/>
-Conflicts are normally much more complex to this, but apply these steps to any issues and they will be easy to resolve<br/>
-"git log", you will see that all of your commits up to the merge are now in your log<br/>


<b>##Branches##</b><br/>
-When you need to work from the same code base but make multiple changes seperate (to not impede other developers) you can use branches<br/>
-"git branch [newBranchName]" to create a new local branch<br/>
-- ** "git branch -r" will list all remote branches<br/>
-- ** Wherever you make this branch, it will take the code up to that point for its contents<br/>
-"git checkout <newBranchName>" to switch to your remote branch<br/>
-Create a new file, add and commit it<br/>
-"git push origin <newBranchName>" this will push up your changes and create a new remote branch<br/>
-Switch back to your master branch ("git checkout master") and note the new file is gone<br/>
-"git fetch" & "git pull origin <newBranchName>", you will see you have now got that extra file<br/>
-"git push origin master", check your log and find the commit message from adding your new file is on the master branch<br/>
-Now that we have everything from that other branch, we don't need it, we need to delete it locally and remotely<br/>
-"git branch -d <newBranchName>" will delete the branch locally<br/>
-"git push origin --delete <newBranchName" will delete the branch remotely<br/>
-- ** There is an old syntax for this which someone will tell you to use "git push origin :<newBranchName>", ignore them<br/>


<b>##Rebasing##</b><br/>
-Rebasing is when we want to change the base of a branch<br/>
-We use it similar to merging, but this gives a cleaner history and doesn't include useless commits<br/>
-Create a new branch, <newBranchName> and add a new file, <newFile> to it<br/>
-Commit and push these files<br/>
-"git rebase master" & "git checkout master" & "git merge <newBranchName>"<br/>
-"git log", note that we have merged in all of the new feature, but don't have any merge commits<br/>

<b>##Useful Tips##</b><br/>
-Make a README.md file for your repo, store useful information like deployment in this file<br/>
-"git --help" is the most useful command you will ever use<br/>